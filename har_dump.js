#!/usr/bin/node

const puppeteer = require('puppeteer');
const PuppeteerHar = require('puppeteer-har');
const hardump = 'results.har';

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const har = new PuppeteerHar(page);
  await har.start({ path: hardump });

  await page.goto('https://filmsbykris.com');

    await har.stop();
    await browser.close();
    dump();
})();

function dump(){
  fs = require('fs')
  fs.readFile(hardump, 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    console.log(data);
  });
}
