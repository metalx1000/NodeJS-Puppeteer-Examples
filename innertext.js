#!/usr/bin/node
const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://filmsbykris.com');
  let v = await page.$eval('#sec_videos', e => e.innerText);
  console.log(v);
  await browser.close();
})();
