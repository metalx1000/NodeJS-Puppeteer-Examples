#!/usr/bin/node
const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://filmsbykris.com');
    const els = await page.$$('a');
    for (el of els) {
      const text = await (await el.getProperty('innerText')).jsonValue();
      console.log(text);
    }

    await browser.close();
})();

