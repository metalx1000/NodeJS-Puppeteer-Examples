#!/usr/bin/node
const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({ headless: true })
  const page = await browser.newPage()
  await page.goto('http://localhost/metalx1000/login.html')
    await page.type('#user', 'metalx1000')
    await page.type('#password', 'goodpassword')
    await page.screenshot({ path: 'login.png' });
    //await page.click('[name="submit"]')
    await page.click('#submit')
    //await page.waitForNavigation()
    await page.screenshot({ path: 'landing.png' });
    browser.close()
})()
