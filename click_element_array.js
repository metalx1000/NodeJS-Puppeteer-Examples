#!/usr/bin/node
const puppeteer = require('puppeteer');

(async () => {
  //const browser = await puppeteer.launch();
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({ width: 1920, height: 1080 });
  await page.goto('https://filmsbykris.com');
    await page.screenshot({ path: 'fbk_1.png' });

    await delay(3000);
    const els = await page.$$('a');
    for (el of els) {
      const text = await (await el.getProperty('innerText')).jsonValue();
      if(text == "GAMES"){
        el.click();
        console.log(text + " clicked");
      }
    }
    
    await delay(1000);
    await page.screenshot({ path: 'fbk_2.png' });
    await browser.close();
})();


function delay(time) {
  return new Promise(function(resolve) {
    setTimeout(resolve, time)
  });
}
