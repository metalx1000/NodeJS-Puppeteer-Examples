#!/usr/bin/node

const puppeteer = require('puppeteer');

(async function main() {
  try {
    const browser = await puppeteer.launch();
    const [page] = await browser.pages();

    await page.goto('https://filmsbykris.com/', { waitUntil: 'networkidle0' });
      const data = await page.evaluate(() => document.querySelector('*').outerHTML);

      console.log(data);

      await browser.close();
  } catch (err) {
    console.error(err);
  }
})();
